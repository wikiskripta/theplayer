<?php

/**
 * All hooked functions used by ThePlayer
 * @ingroup Extensions
 * @author Josef Martiňák
 */

class ThePlayerHooks {

	/**
	 * Default params
	 */
	private static $videoWidth = 700;
	private static $videoHeight = 400;
	private static $audioWidth = 300;
	private static $audioHeight = 180;

	/**
	 * Set up the parser hooks
	 * @param object $parser: instance of Parser object
	 * @return Boolean: true
	 */
	public static function registerParserHook( &$parser ) {
		$parser->setHook( 'mediaplayer', array( 'ThePlayerHooks', 'videoRender' ) );
		$parser->setHook( 'mp3player', array( 'ThePlayerHooks', 'audioRender' ) );
		return true;
	}


	/**
	 * Callback function for registerParserHook
	 * @param string $input: user-supplied input, unused
	 * @param array $args: user-supplied arguments
	 * @param object $parser: instance of Parser, unused
	 * @return String: HTML
	 */
	public static function videoRender( $input, $args, $parser ) {
		global $wgServer;
		if( !empty( $input ) ) {
			$input = htmlspecialchars( $input, ENT_QUOTES );
			$input = preg_replace( "/watch\?v=/", "embed/", $input ); // youtube
		} 
		else {
			$input = '';
		}

		if( !empty( $args['width'] ) && preg_match("/^[0-9]*$/",$args['width']) ) {
			ThePlayerHooks::$videoWidth = $args['width'];
		}

		if( !empty( $args['height'] ) && preg_match("/^[0-9]*$/",$args['height']) ) {
			ThePlayerHooks::$videoHeight = $args['height'];
		}

		if( !empty( $args['type'] ) && preg_match("/^[file|wiki|stream]$/",$args['type']) ) {
			$type = $args['type'];
		}
		elseif( empty( $args['type'] ) && preg_match( '/[File:|Soubor:]/', $input )  && !preg_match( '/http/', $input ) ) {
			$type = 'wiki';
		}
		else $type = 'stream';

		// float
		if( !empty( $args['align'] ) && in_array($args['align'],array("left","right")) ) {
			$output = "<div style='float:" . $args['align'] . ";'>";
		}
		else $output = "";
		switch( $type ) {
			case 'stream':
			// external (youtube)
			$output .= '<iframe width="' . ThePlayerHooks::$videoWidth . '" height="' . ThePlayerHooks::$videoHeight . '" ';
			$output .= 'src="' . $input . '" frameborder="0" allowfullscreen></iframe>';
			break;

			case 'wiki':
			// wiki file page
			$tmp = preg_replace( '/(File|Soubor):/', '', $input );
			$json = file_get_contents( "$wgServer/api.php?action=query&titles=File:$tmp&prop=imageinfo&iiprop=url&format=json" );
			$obj = (object)json_decode($json);
			$o2 = new stdClass;
			$o2 = $obj->query->pages;
			$url = '';
			foreach( $o2 as $pg ) {
				$url = $pg->imageinfo[0]->url;
				break;
			}
			$output .= '<video controls>\n';
			$output .= '<source src="' . $url . '">\n' . wfMessage('theplayer-error') . '\n</video>';
			break;

			case 'file':
			// filepath
			$output .= '<video controls>\n';
			$output .= '<source src="' . $input . '">\n' . wfMessage('theplayer-error') . '\n</video>';
			break;
		}
		if( !empty( $args['align'] ) && in_array($args['align'],array("left","right")) ) {
			$output .= "</div>";
		}
		return $output;
	}


	/**
	 * Callback function for registerParserHook
	 * @param string $input: user-supplied input, unused
	 * @param array $args: user-supplied arguments, unused
	 * @param object $parser: instance of Parser, unused
	 * @return String: HTML
	 */
	public static function audioRender( $input, $args, $parser ) {
		global $wgServer;
		if( !empty( $input ) ) {
			$input = htmlspecialchars( $input, ENT_QUOTES );
			$input = preg_replace( "/watch\?v=/", "embed/", $input ); // youtube
		} 
		else {
			$input = '';
		}

		if( !empty( $args['width'] ) && preg_match("/^[0-9]*$/",$args['width']) ) {
			ThePlayerHooks::$audioWidth = $args['width'];
		}

		if( !empty( $args['height'] ) && preg_match("/^[0-9]*$/",$args['height']) ) {
			ThePlayerHooks::$audioHeight = $args['height'];
		}

		if( !empty( $args['type'] ) && preg_match("/^[file|wiki|stream]$/",$args['type']) ) {
			$type = $args['type'];
		}
		elseif( empty( $args['type'] ) && preg_match( '/[File:|Soubor:]/', $input )  && !preg_match( '/http/', $input ) ) {
			$type = 'wiki';
		}
		else $type = 'stream';

		switch( $type ) {
			case 'stream':
			// external (youtube, soundcloud)
			$output = '<iframe width="' . ThePlayerHooks::$audioWidth . '" height="' . ThePlayerHooks::$audioHeight . '" ';
			$output .= 'src="' . $input . '" frameborder="0" allowfullscreen></iframe>';
			break;

			case 'wiki':
			// wiki file page
			$tmp = preg_replace( '/(File|Soubor):/', '', $input );
			$json = file_get_contents( "$wgServer/api.php?action=query&titles=File:$tmp&prop=imageinfo&iiprop=url&format=json" );
			$obj = (object)json_decode($json);
			$o2 = new stdClass;
			$o2 = $obj->query->pages;
			$url = '';
			foreach( $o2 as $pg ) {
				$url = $pg->imageinfo[0]->url;
				break;
			}
			$output = '<audio controls>\n';
			$output .= '<source src="' . $url . '">\n' . wfMessage('theplayer-error') . '\n</audio>';
			break;

			case 'file':
			// filepath
			$output = '<audio controls>\n';
			$output .= '<source src="' . $input . '">\n' . wfMessage('theplayer-error') . '\n</audio>\n';
			break;
		}
		return $output;
	}
}