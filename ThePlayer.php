<?php

/**
 * The Player Mediawiki extension
 * embed media files into an wiki article.
 * Define tags <mediaplayer> and <mp3player> for compatibility with Extension:MediaPlayer
 * @ingroup Extensions
 * @author Josef Martiňák
 */

if (!function_exists('wfLoadExtension')) {
	die('ThePlayer extension requires MediaWiki 1.25 or newer.');
}

wfLoadExtension('ThePlayer');