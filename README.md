# ThePlayer

Mediawiki extension.

## Description

* Version 1.0
* _ThePlayer_ can embed video and mp3 into an wiki article.
* Syntax

```xml
<mediaplayer width="" height="" type="stream|wiki|file">URL</mediaplayer>
<mp3player width="" height="" type="stream|wiki|file">URL</mp3layer>
```

* If _type_ is not set, the extension tries to guess
    * _stream_: URL of streamed file from embedding iframe code. In case of Youtube, URL of displayed video is sufficient.
	* _wiki_: file uploaded on the same wiki.
	* _file_: file anywhere on the internet.
* _Width_ and _height_ can be adjusted with `type="stream"` (optional).
* A modern HTML5 browser is required in case of non-streamed file. Otherwise a request for updating the browser is displayed.

## Installation

* Make sure you have MediaWiki 1.25+ installed.
* Download and place the extension's folder to your /extensions/ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'ThePlayer' )`;

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2021 First Faculty of Medicine, Charles University